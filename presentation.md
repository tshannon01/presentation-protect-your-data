# Protect your Data and Devices

## So you dropped your phone in a crocodile pit, what happens next?

### Replace the device
Is it time to go shopping, or do you already have a backup phone… maybe you can wait until your insurance handles all the paperwork?

### What about the data?
Are your photos on there? Your emails? Your contacts? Your passwords?

Can you get them back?

### Criminals not crocodiles
What if your crazy ex got your phone, the one who still knows the password to your emails? Or maybe it's just the scary Russian/Chinese hacker you're worried about. How much damage can they do if they get your phone.

### Your computer
Maybe you don't care about your phone that much because you do all your work on your computer. Things happen, how long would it take you to get your bills paid, or to get your clients to pay their bills if your main computer goes down?

## My Setup
Depending on what you do, where, and on what devices, your solution is going to be a little different. Let me run you through my setup.

I live on these devices, I take all this stuff way to seriously, but this is what I've got and why.

- Passwords
    - Stored in a password managers, secured with 2FA
- Two Factor Authentication (2FA)
    - Authy on both my phones
        - Less spoofable than sms or email 2FA
        - Two phones for redundant secure access to accounts
- Online data
    - Lightspeed (the main piece of software I use at work)
        - All it's data is securely stored on a network server, I can access this from any almost any macOS machine with little downtime
    - Emails
        - Multiple accounts secured with 2FA
        - Accessed through web interface to avoid any custom configuration or hassle on my machines
    - Project File (like this presentation)
        - Version control is king, I use git
        - For the important projects I sync them to my cloud services
- Photos (nothing to worry about because)
    - Personal
        - Uploaded to OneDrive automatically, mostly of cool dogs
    - Work
        - Uploaded to Slack as I work
- Contacts
    - Email
        - Automatically stored in each of their accounts
    - Phone
        - Synced to iCloud

### But there's no backup?
I don't backup my machines, I've configured my work flows and devices to have redundancies built into them automatically. That way I can't forget to do it.

## Designing a solution for you

### Step One - Identify your needs
How you pay your bills, how you work, what you can't be without for more than a day. Next up are the things you need, but not desperately, this will end up in the second tier. And lastly are things you like, things you want, but don't need; they make up the final tier.

### Step Two - Protect your critical infrastructure
The first tier is where you focus your efforts and resources on. For me, that's my phone, without it and the passwords it stores I could get locked out of everything from my bank to Netflix, everything would breakdown. To protect this I have -

    1. A second phone
    2. A password management service that synchronizes between both devices automatically
    3. A 2FA app that synchronizes between both devices automatically

All of the critical functions of my phone, I can do from either device, without interruption. This is a no down time redundancy.

### Step Three - Protect your important infrastructure
The second tier generally doesn't need such immediate responses, for me this is my laptop and it's data. I work better and faster with my unique setup, but I can function just fine with the machines and setup provided by macs4u. To protect this layer I have -

    1. Private cloud storage for my personal data, automatically syncing
    2. Private cloud storage for my work data, such as this presentation, automatically syncing
    3. My software license keys are available from my password manager
    4. I keep a configuration file (cloud synced) that includes most of the software and settings I use to make my machine 'mine'

With these systems, I have no pressure to replace my machine other than my own desires. I can access all the above data from web interfaces and work on them almost anywhere. When I broke the screen on my laptop and was using a work laptop, there was minimal interruptions. I didn't install my full configuration as the machine wasn't mine, but at worst it slowed me down. Because the services sync, when I got my laptop back everything was already waiting for me.

## Don't trust the cloud?
The value proposition for cloud services is generally redundancy, security, accessibility, and in many cases, lower costs.

### Redundancy
They provide redundancy in that the data centers have large-scale enterprise grade backup solutions. Loosing data is generally quite hard, even if you delete it most services have a grace period (e.g., 20 days or 30 days) to recover it. They provide me redundancy by automatically syncing my files to them, and leveraging their protections so I don't have to.

### Security
I can't think of a cloud service that doesn't use encryption to access the data. Most of the business grade solutions have optional integrations of more advanced encryption for compliance needs. Most are governed or effected by laws like the EU's GDPR which are quite strongly worded in favor of the individual rights.

### Accessibility
This is where I find the most benefit, ubiquitous access to the latest versions of everything that I have. From my phone, to my laptop or iPad, I have it all at my fingertips, always. But hidden behind this are the access controls, I can share access to single files or whole folders with my colleagues, and I'll always have access to the latest version that we're working on together. And when they don't need it anymore, I just revoke their access, no need to worry.

### Costs
I pay around $120 a year for all of my cloud solutions, that include Office (Word, PowerPoint et al.). What this saves me is much higher. It saves me scrounging around for the right file. It saves me from forgetting to backup my machine. It saves me from having an excellent idea on the way home and not having a way of using it. I can provide my brand of weird enthusiastic knowledge to the world better because of the access I have. It saves me from worrying about how long it would take me to get up and running should something bad happen, because I know I can be productive in minutes and fully running in hours. So I pay them their money.